/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.5.54-0ubuntu0.14.04.1 : Database - riparadise
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`riparadise` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `riparadise`;

/*Table structure for table `tb_comment` */

DROP TABLE IF EXISTS `tb_comment`;

CREATE TABLE `tb_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_comment` */

insert  into `tb_comment`(`id`,`post_id`,`user_id`,`comment`,`reg_date`) values (4,5,3,'Hi','2017-03-31 16:32:20'),(5,5,3,'Hi','2017-03-31 16:37:29'),(6,5,3,'Hi','2017-03-31 16:45:13'),(7,12,3,'Hi\nHow are u?\n','2017-03-31 16:47:28'),(8,12,3,'Ok\nLooks good','2017-04-01 01:45:16'),(9,12,4,'good morning\nHow are you today??','2017-04-01 01:56:43'),(11,11,3,'Good tree\nThe\n','2017-04-02 22:49:26'),(12,15,4,'Good\nIt\'s very nice\nI like this machine\nThanks','2017-04-03 13:27:52'),(13,7,5,'Wow','2017-04-03 17:16:43'),(14,10,5,'Hey ','2017-04-03 17:17:30'),(15,16,5,'I see','2017-04-04 04:07:52'),(16,16,5,'Hmmm','2017-04-04 04:08:01'),(17,13,5,'This is working it seems ','2017-04-04 04:08:13'),(18,16,5,'','2017-04-04 22:46:54'),(19,16,5,'','2017-04-04 22:47:10'),(20,11,3,'Ok','2017-04-07 02:02:28'),(21,11,3,'Ok','2017-04-07 02:02:36'),(22,11,3,'Ok','2017-04-07 02:02:46'),(23,11,3,'Very good','2017-04-07 02:03:21'),(24,11,4,'Wow\nThat\'s a great\nI like it\nThanks','2017-04-07 02:04:23'),(25,11,4,'Everybody can know it is very great tree, so that I am also like it. Test2 published wonderful tree.\n\nHigh recommend\n','2017-04-07 02:05:10'),(26,11,5,'I see','2017-04-07 07:50:32'),(27,11,5,'','2017-04-07 08:07:48');

/*Table structure for table `tb_like` */

DROP TABLE IF EXISTS `tb_like`;

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '-1',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `tb_like` */

insert  into `tb_like`(`id`,`user_id`,`post_id`,`type`,`reg_date`) values (1,3,5,4,'2017-04-07 01:43:53'),(2,3,12,3,'2017-04-07 01:41:57'),(3,4,12,2,'2017-04-03 09:25:52'),(4,3,1,4,'2017-04-02 18:02:48'),(5,3,12,3,'2017-04-07 01:41:57'),(6,3,11,3,'2017-04-02 23:25:11'),(7,3,6,1,'2017-04-02 22:55:32'),(8,3,8,2,'2017-04-03 22:46:42'),(9,3,10,1,'2017-04-02 23:26:28'),(10,3,9,3,'2017-04-02 23:26:41'),(11,3,7,1,'2017-04-02 23:37:16'),(12,4,6,5,'2017-04-03 12:28:09'),(13,4,9,4,'2017-04-03 03:31:31'),(14,4,10,4,'2017-04-03 12:27:33'),(15,4,5,0,'2017-04-03 03:30:57'),(16,4,8,2,'2017-04-05 09:17:06'),(17,3,13,2,'2017-04-07 01:38:19'),(18,4,13,4,'2017-04-06 20:18:35'),(19,4,7,5,'2017-04-03 12:28:01'),(20,4,11,1,'2017-04-03 12:51:16'),(21,3,15,1,'2017-04-03 13:21:03'),(22,4,15,4,'2017-04-03 13:24:32'),(23,5,5,3,'2017-04-03 22:30:59'),(24,5,6,1,'2017-04-03 22:30:41'),(25,5,10,2,'2017-04-03 17:17:23'),(26,5,16,3,'2017-04-04 04:08:59'),(27,5,9,5,'2017-04-03 22:30:25'),(28,5,8,3,'2017-04-03 22:30:33'),(29,5,7,0,'2017-04-03 22:30:38'),(30,5,11,2,'2017-04-07 08:07:26'),(31,5,12,0,'2017-04-07 07:51:30'),(32,5,13,0,'2017-04-03 22:31:17'),(33,5,15,1,'2017-04-03 22:31:28');

/*Table structure for table `tb_post` */

DROP TABLE IF EXISTS `tb_post`;

CREATE TABLE `tb_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `question1` varchar(255) NOT NULL,
  `question2` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `tb_post` */

insert  into `tb_post`(`id`,`user_id`,`caption`,`question1`,`question2`,`photo_url`,`reg_date`) values (5,3,'Test caption','3 year','it was old','http://54.71.163.255/uploadfiles/user/2017/03/pic14908427963.png','2017-03-30 02:59:56'),(6,3,'Tower','3','broken','http://54.71.163.255/uploadfiles/user/2017/03/pic14908455545.png','2017-03-30 03:45:54'),(7,3,'Autumn tree','3','never','http://54.71.163.255/uploadfiles/user/2017/03/pic14908457404.png','2017-03-30 03:49:00'),(8,4,'Simulator1','3 years','none','http://54.71.163.255/uploadfiles/user/2017/03/pic14908617433.png','2017-03-30 08:15:43'),(9,4,'Simulator2','3 years','none','http://54.71.163.255/uploadfiles/user/2017/03/pic14908617796.png','2017-03-30 08:16:19'),(10,4,'simu3','3 years','none','http://54.71.163.255/uploadfiles/user/2017/03/pic14908618526.png','2017-03-30 08:17:32'),(11,3,'Fhh','gg','dx','http://54.71.163.255/uploadfiles/user/2017/03/pic14908680954.png','2017-03-30 10:01:35'),(12,5,'The best way to describe this photo is... well this photo has no description. I\'m just seeing how much I can type before this section dissap','2017','mnnnbbbbbbbb','http://54.71.163.255/uploadfiles/user/2017/03/pic14908808722.png','2017-03-30 13:34:32'),(13,4,'Water fall','2017','mountain','http://54.71.163.255/uploadfiles/user/2017/04/pic14911905601.png','2017-04-03 03:36:00'),(15,3,'Slot machine','2015','oma','http://54.71.163.255/uploadfiles/user/2017/04/pic14912256420.png','2017-04-03 13:20:42'),(16,5,'R.i.p book','2014','idk','http://54.71.163.255/uploadfiles/user/2017/04/pic14912399592.png','2017-04-03 17:19:19'),(17,5,'Rip','2000','unknown ','http://54.71.163.255/uploadfiles/user/2017/04/pic14915526044.png','2017-04-07 08:10:04');

/*Table structure for table `tb_share` */

DROP TABLE IF EXISTS `tb_share`;

CREATE TABLE `tb_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_share` */

insert  into `tb_share`(`id`,`user_id`,`post_id`,`reg_date`) values (1,3,12,'2017-04-02 07:39:50'),(2,3,1,'2017-04-02 14:26:45'),(3,3,8,'2017-04-02 23:04:30'),(4,5,10,'2017-04-03 17:17:59'),(5,5,16,'2017-04-03 20:21:32'),(6,3,15,'2017-04-03 22:06:56'),(7,3,6,'2017-04-03 22:48:04'),(8,3,7,'2017-04-03 22:49:20'),(9,3,5,'2017-04-03 22:53:35'),(10,3,13,'2017-04-03 22:57:59'),(11,3,11,'2017-04-03 23:01:53'),(12,3,16,'2017-04-04 02:59:42'),(13,3,9,'2017-04-04 03:33:50'),(14,5,11,'2017-04-07 07:53:38');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(512) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`user_name`,`email`,`password`,`birthday`,`photo_url`,`reg_date`) values (3,'test01','test01@gmail.com','1234567890','30 Mar 2017','http://54.71.163.255/uploadfiles/user/2017/03/photo14908271126.png','2017-03-29 22:38:32'),(4,'test02','test02@gmail.com','123456','21 Feb 2017','http://54.71.163.255/uploadfiles/user/2017/03/photo14908280127.png','2017-03-29 22:53:32'),(5,'Riparadise','emaxwell015@gmail.com','Eltreign922','25 Jul 1987','http://54.71.163.255/uploadfiles/user/2017/03/photo14908806847.png','2017-03-30 13:31:24');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
