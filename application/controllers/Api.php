<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
     
     function register() {
         
         $result = array();
         
         $username = $_POST['username'];         
         $email = $_POST['email'];         
         $password = $_POST['password'];         
         $birthday = $_POST['birthday'];         
         
         $usernameExist = $this->api_model->usernameExist($username);         

         if($usernameExist > 0) { // username already exist
             $result['error'] = "Username already exist.";
             $this->doRespond(201, $result);
             return;
         }
         
         $emailExist = $this->api_model->emailExist($email);         

         if($emailExist > 0) { // email already exist
             $result['error'] = "Email address already exist.";
             $this->doRespond(202, $result);
             return;
         }

         
         if(!is_dir("uploadfiles/user/")) {
            mkdir("uploadfiles/user/");
         }
         $upload_path = "uploadfiles/user/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path; 

        // Upload file.  
        $image_name = "photo";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $result['id'] = $this->api_model->register($username, $email, $password, $birthday, $file_url);
            $result['photo_url'] = $file_url;
                        
            $this->doRespondSuccess($result);
            return;

        } else {
            $result['error'] = "Failed to upload file.";
            $this->doRespond(203, $result);// upload fail
            return;
        }
        
        $this->doRespondSuccess($result);          
     }
     
     function login() {
         
         $result = array();
         
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $usernameExist = $this->api_model->emailExist($email);         

         if($usernameExist == 0) { // username already exist
             $result['error'] = "Unregistered Email address.";
             $this->doRespond(204, $result);
             return;
         }
         
         $user = $this->db->where('email', $email)->get('tb_user')->row();
         
         $pwd = $user->password;

         //if (!password_verify($password, $pwd)){  // wrong password.
         if ($password != $pwd) {
             $result['error'] = "Wrong password.";
             $this->doRespond(205, $result);
             return;

         } else {
             
             $result = array('id' => $user->id,
                             'username' => $user->user_name,
                             'email' => $user->email,
                             'birthday' => $user->birthday,
                             'photo_url' => $user->photo_url);
         }
                           
         $this->doRespondSuccess($result); 
     }
     
     function resetPassword() {
         
         $result = array();
         
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $usernameExist = $this->api_model->emailExist($email);         

         if($usernameExist == 0) { // username already exist
             $result['error'] = "Unregistered Email address.";
             $this->doRespond(204, $result);
             return;
         }
         
         $this->api_model->resetPassword($email, $password);
         $this->doRespondSuccess($result);         
     } 
     
     function post() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];         
         $caption = $_POST['caption'];         
         $question1 = $_POST['question1'];
         $question2 = $_POST['question2'];
         
         if(!is_dir("uploadfiles/post/")) {
            mkdir("uploadfiles/post/");
         }
         $upload_path = "uploadfiles/user/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path; 

        // Upload file.  
        $image_name = "pic";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $result['id'] = $this->api_model->post($user_id, $caption, $question1, $question2, $file_url);            
            $this->doRespondSuccess($result);
            return;

        } else {
            $result['error'] = "Failed to upload file.";
            $this->doRespond(205, $result);// upload fail
            return;
        }
        
        $this->doRespondSuccess($result);           
     }
     
     function newsfeed() {
         
         $result = array();
         $user_id = $_POST['user_id'];
         $page_index = $_POST['page_index'];
         
         $result['newsfeed'] = $this->api_model->newsfeed($user_id, $page_index);
         
         $this->doRespondSuccess($result);
     }
     
     function setComment() {
         
         $result = array();
         
         $post_id = $_POST['post_id'];
         $user_id = $_POST['user_id'];
         $comment = $_POST['comment'];
         
         $result['id'] = $this->api_model->setComment($post_id, $user_id, $comment);
         $this->doRespondSuccess($result); 
     }
     
     function getComment() {
         
         $result = array();
         $post_id = $_POST['post_id'];
         
         $result['comments'] = $this->api_model->getComment($post_id);
         
         $this->doRespondSuccess($result);
     }
     
     function setLike() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $post_id = $_POST['post_id'];
         $type = $_POST['type'];
         
         $this->api_model->setLike($user_id, $post_id, $type);
         
         $this->doRespondSuccess($result);         
     }
     
     function setShare() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $post_id = $_POST['post_id'];
         
         $this->api_model->setShare($user_id, $post_id);
         
         $this->doRespondSuccess($result);    
         
     }
}
?>
