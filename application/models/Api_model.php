<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function usernameExist($name) {

        $this->db->where('user_name', $name);         
        return $this->db->get('tb_user')->num_rows();
    }

    function emailExist($email) {

        $this->db->where('email', $email);
        return $this->db->get('tb_user')->num_rows();
    }
    
    function register($username, $email, $password, $birthday, $file_url) {

        $this->db->set('user_name', $username);
        $this->db->set('email', $email);        
        $this->db->set('password', $password);
        $this->db->set('birthday', $birthday);
        $this->db->set('photo_url', $file_url);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function updatePhoto($user_id, $file_url) {
        
        $this->db->where('id', $user_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_user');
    }
    
    function resetPassword($email, $password) {
        
        $this->db->where('email', $email);
        $this->db->set('password', $password);
        $this->db->update('tb_user');
    }
    
    function post($user_id, $caption, $question1, $question2, $file_url) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('caption', $caption);        
        $this->db->set('question1', $question1);
        $this->db->set('question2', $question2);
        $this->db->set('photo_url', $file_url);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_post');
        return $this->db->insert_id();
    }
    
    function newsfeed($user_id, $page_index) {
        
        $result = array();
        
        $start =  ($page_index - 1) * 30;        
        $this->db->limit(30, $start);
        $this->db->order_by('reg_date', 'DESC');
        $query = $this->db->get('tb_post');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $post) {
                
                $user = $this->db->where('id', $post->user_id)->get('tb_user')->row();
                $share_count = $this->db->where('post_id', $post->id)->get('tb_share')->num_rows();
                $comment_count = $this->db->where('post_id', $post->id)->get('tb_comment')->num_rows();
                
                $this->db->order_by('reg_date', 'DESC');
                $this->db->where('post_id', $post->id);
                $like_query = $this->db->get('tb_like');
                $like_count = $like_query->num_rows();
                
                $my_like_type = -1;
                $recent_like_type = -1;
                if ($like_count > 0) {
                    foreach($like_query->result() as $like) {
                        
                        if ($like->user_id == $user_id) {
                            
                            $my_like_type = $like->type;
                                                        
                        } else if ($recent_like_type == -1) 
                            $recent_like_type = $like->type;
                        }
                        
                        if ($my_like_type != -1 && $recent_like_type != -1) 
                            continue;                         
                    }
                
                $arr = array('id' => $post->id,
                             'user_id' => $post->user_id,
                             'username' => $user->user_name,
                             'photo_url' => $user->photo_url,
                             'caption' => $post->caption,
                             'question1' => $post->question1,
                             'question2' => $post->question2,
                             'file_url' => $post->photo_url,
                             'like_count' => $like_count,
                             'comment_count' => $comment_count,
                             'share_count' => $share_count,
                             'my_like_type' => $my_like_type,
                             'recent_like_type' => $recent_like_type,
                             'reg_date' => $post->reg_date);
                array_push($result, $arr);
            } 
        }
        
        return $result;
    }
    
    function setComment($post_id, $user_id, $comment) {
        
        $this->db->set('post_id', $post_id);
        $this->db->set('user_id', $user_id);
        $this->db->set('comment', $comment);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_comment');
        return $this->db->insert_id();
    }
    
    function getComment($post_id) {
        
        $result = array();
        
        $this->db->order_by('reg_date', 'DESC');
        $this->db->where('post_id', $post_id);
        $query = $this->db->get('tb_comment');
        
        if($query->num_rows() > 0) {
            
            foreach($query->result() as $comment) {
                
                $user = $this->db->where('id', $comment->user_id)->get('tb_user')->row();
                
                $arr = array('id' => $comment->id,
                             'user_id' => $comment->user_id,
                             'username' => $user->user_name,
                             'photo_url' => $user->photo_url,
                             'comment' => $comment->comment,
                             'reg_date' => $comment->reg_date);
                             
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    
    function setLike($user_id, $post_id, $type) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('post_id', $post_id);
        $exsit = $this->db->get('tb_like')->num_rows();
        
        if ($exsit > 0) {
            
            $this->db->where('user_id', $user_id);
            $this->db->where('post_id', $post_id);
            $this->db->set('type', $type);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->update('tb_like');
            
            
        } else {
            
            $this->db->set('user_id', $user_id);
            $this->db->set('post_id', $post_id);
            $this->db->set('type', $type);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->insert('tb_like');
            $this->db->insert_id();           
            
        }                          
    }
    
    function setShare($user_id, $post_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('post_id', $post_id);
        $exsit = $this->db->get('tb_share')->num_rows();
        
        if ($exsit == 0) {
            
            $this->db->set('user_id', $user_id);
            $this->db->set('post_id', $post_id);            
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->insert('tb_share');
            $this->db->insert_id();
        } 
    }
}
?>
